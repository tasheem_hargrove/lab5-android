package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class Flashcard(
    @PrimaryKey val id: Long,
    var question: String,
    var answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard(i.toLong(), "Term $i", "Def $i"))
            }
            return cards
        }
    }
}