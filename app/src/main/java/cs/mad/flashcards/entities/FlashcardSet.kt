package cs.mad.flashcards.entities

import androidx.room.*

@Entity(tableName = "flashcard_set")
data class FlashcardSet(
    @PrimaryKey val id: Long,
    val title: String
) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet(i.toLong(),"Set $i"))
            }
            return sets
        }
    }
}