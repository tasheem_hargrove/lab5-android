package cs.mad.flashcards.dao

import androidx.room.*
import cs.mad.flashcards.entities.FlashcardSet

@Dao
interface FlashcardSetDAO {
    @Query("select * from flashcard_set")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(card: FlashcardSet)

    @Insert
    suspend fun insertAll(cards: List<FlashcardSet>)

    @Update
    suspend fun update(card: FlashcardSet)

    @Delete
    suspend fun delete(card: FlashcardSet)

    @Query("delete from flashcard_set")
    suspend fun deleteAll()
}