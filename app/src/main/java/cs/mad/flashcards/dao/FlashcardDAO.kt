package cs.mad.flashcards.dao

import androidx.room.*
import cs.mad.flashcards.entities.Flashcard

@Dao
interface FlashcardDAO {
    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(card: Flashcard)

    @Insert
    suspend fun insertAll(cards: List<Flashcard>)

    @Update
    suspend fun update(card: Flashcard)

    @Delete
    suspend fun delete(card: Flashcard)

    @Query("delete from flashcard")
    suspend fun deleteAll()
}