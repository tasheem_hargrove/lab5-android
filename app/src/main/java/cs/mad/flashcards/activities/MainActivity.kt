package cs.mad.flashcards.activities

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.dao.FlashcardSetDAO
import cs.mad.flashcards.database.FlashcardDatabase
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    public lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var flashcardSetDao: FlashcardSetDAO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardDatabase::class.java, FlashcardDatabase.databaseName
        ).build()
        flashcardSetDao = db.flashcardSetDao()

        prefs = getSharedPreferences("MADFlashcards", MODE_PRIVATE)

        binding.flashcardSetList.adapter = FlashcardSetAdapter(flashcardSetDao)

        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet(Random.nextLong(),"test"))
            binding.flashcardSetList.smoothScrollToPosition(
                (binding.flashcardSetList.adapter as FlashcardSetAdapter)
                    .itemCount - 1)
        }
    }

    override fun onResume() {
        super.onResume()

        val adapter = binding.flashcardSetList.adapter as FlashcardSetAdapter
        adapter.rePopulateCollection()
    }
}