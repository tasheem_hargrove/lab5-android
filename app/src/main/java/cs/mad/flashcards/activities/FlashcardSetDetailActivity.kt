package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.dao.FlashcardDAO
import cs.mad.flashcards.database.FlashcardDatabase
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.runOnIO
import kotlin.random.Random

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var flashcardDao: FlashcardDAO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardDatabase::class.java, FlashcardDatabase.databaseName
        ).build()
        flashcardDao = db.flashcardDao()

        binding.flashcardList.adapter = FlashcardAdapter(flashcardDao)

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem()
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {
            val targetID = intent.getLongExtra("setID", -1)
            val fsDao = db.flashcardSetDao()

            runOnIO {
                val sets = fsDao.getAll()

                var targetSet: FlashcardSet? = null
                for (set in sets) {
                    if(set.id == targetID) {
                        targetSet = set
                        break
                    }
                }

                if (targetSet != null) {
                    fsDao.delete(targetSet)
                    val fsAdapter = FlashcardSetAdapter(db.flashcardSetDao())
                    fsAdapter.removeItem(targetSet)
                }
            }

            finish()
        }
    }
}