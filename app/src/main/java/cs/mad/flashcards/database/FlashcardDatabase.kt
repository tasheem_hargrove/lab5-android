package cs.mad.flashcards.database

import androidx.room.*
import cs.mad.flashcards.dao.FlashcardDAO
import cs.mad.flashcards.dao.FlashcardSetDAO
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

@Database(entities = [Flashcard::class, FlashcardSet::class], version = 1)
abstract class FlashcardDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "FLASHCARD_DATABASE"
    }

    abstract fun flashcardDao(): FlashcardDAO

    abstract fun flashcardSetDao(): FlashcardSetDAO
}