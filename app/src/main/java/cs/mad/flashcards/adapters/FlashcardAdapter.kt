package cs.mad.flashcards.adapters

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.dao.FlashcardDAO
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.runOnIO
import kotlin.random.Random

class FlashcardAdapter(flashcardDAO: FlashcardDAO) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = mutableListOf<Flashcard>()
    private val dao = flashcardDAO

    init {
        runOnIO {
            this.dataSet.addAll(dao.getAll())
        }
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
        viewHolder.itemView.setOnClickListener {
            AlertDialog.Builder(it.context)
                    .setTitle(item.question)
                    .setMessage(item.answer)
                    .setPositiveButton("Done") { _,_ -> }
                    .setNeutralButton("Edit") { _, _ -> showEditDialog(it.context, item) }
                    .create()
                    .show()
        }
        viewHolder.itemView.setOnLongClickListener {
            showEditDialog(it.context, item)
            true
        }
    }

    private fun showEditDialog(context: Context, flashcard: Flashcard) {
        val customTitle = EditText(context)
        val customBody = EditText(context)
        customTitle.setText(flashcard.question)
        customTitle.textSize = 20 * context.resources.displayMetrics.scaledDensity
        customBody.setText(flashcard.answer)

        AlertDialog.Builder(context)
                .setCustomTitle(customTitle)
                .setView(customBody)
                .setPositiveButton("Done") {
                        dialogInterface: DialogInterface, _ ->
                    val updatedFlashcard = Flashcard(flashcard.id,
                    customTitle.text.toString(), customBody.text.toString())

                    updateItem(updatedFlashcard)
                    dialogInterface.dismiss()
                }
                .setNegativeButton("Delete") {
                        dialogInterface: DialogInterface,_ ->
                    removeItem(flashcard)
                    dialogInterface.dismiss()
                }
                .create()
                .show()
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    private fun rePopulateCollection() {
        dataSet.clear()
        runOnIO {
            dataSet.addAll(dao.getAll())
        }
    }

    fun addItem() {
        val card = Flashcard(Random.nextLong(), "question ${dataSet.count() + 1}",
            "answer ${dataSet.count() + 1}")

        runOnIO {
            dao.insert(card)
        }
        rePopulateCollection()
        notifyDataSetChanged()
    }

    fun triggerChange() {
        notifyDataSetChanged()
    }

    fun updateItem(it: Flashcard) {
        runOnIO {
            dao.update(it)
        }
        rePopulateCollection()
        notifyDataSetChanged()
    }

    fun removeItem(it: Flashcard) {
        runOnIO {
            dao.delete(it)
        }
        rePopulateCollection()
        notifyDataSetChanged()
    }
}