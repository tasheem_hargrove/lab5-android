package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.dao.FlashcardSetDAO
import cs.mad.flashcards.databinding.ItemFlashcardSetBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.runOnIO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class FlashcardSetAdapter(flashcardSetDAO: FlashcardSetDAO) :
    RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {

    private val dataSet = mutableListOf<FlashcardSet>()
    private val dao = flashcardSetDAO

    init {
        runOnIO {
            dataSet.addAll(dao.getAll())
        }
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(bind: ItemFlashcardSetBinding) : RecyclerView.ViewHolder(bind.root) {
        val binding: ItemFlashcardSetBinding = bind
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFlashcardSetBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        binding.root.minimumHeight = viewGroup.height / 5
        return ViewHolder(binding)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.binding.flashcardSetTitle.text = item.title
        viewHolder.binding.root.setOnClickListener {
            val intent = Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java)
            intent.putExtra("setID", dataSet[position].id)
            viewHolder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    public fun rePopulateCollection() {
        dataSet.clear()
        runOnIO {
            dataSet.addAll(dao.getAll())
        }
        notifyDataSetChanged()
    }

    fun addItem(it: FlashcardSet) {
        runOnIO { dao.insert(it) }
        rePopulateCollection()
    }

    fun removeItem(it: FlashcardSet) {
        runOnIO { dao.delete(it) }
        rePopulateCollection()
    }
}