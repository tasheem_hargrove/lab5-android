package cs.mad.flashcards.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001eB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\n\u001a\u00020\u000bJ\b\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\rH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\rH\u0016J\b\u0010\u0015\u001a\u00020\u000bH\u0002J\u000e\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\tJ\u0018\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\tH\u0002J\u0006\u0010\u001c\u001a\u00020\u000bJ\u000e\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\tR\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcs/mad/flashcards/adapters/FlashcardAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcs/mad/flashcards/adapters/FlashcardAdapter$ViewHolder;", "flashcardDAO", "Lcs/mad/flashcards/dao/FlashcardDAO;", "(Lcs/mad/flashcards/dao/FlashcardDAO;)V", "dao", "dataSet", "", "Lcs/mad/flashcards/entities/Flashcard;", "addItem", "", "getItemCount", "", "onBindViewHolder", "viewHolder", "position", "onCreateViewHolder", "viewGroup", "Landroid/view/ViewGroup;", "viewType", "rePopulateCollection", "removeItem", "it", "showEditDialog", "context", "Landroid/content/Context;", "flashcard", "triggerChange", "updateItem", "ViewHolder", "app_debug"})
public final class FlashcardAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<cs.mad.flashcards.adapters.FlashcardAdapter.ViewHolder> {
    private final java.util.List<cs.mad.flashcards.entities.Flashcard> dataSet = null;
    private final cs.mad.flashcards.dao.FlashcardDAO dao = null;
    
    public FlashcardAdapter(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.dao.FlashcardDAO flashcardDAO) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public cs.mad.flashcards.adapters.FlashcardAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup viewGroup, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.adapters.FlashcardAdapter.ViewHolder viewHolder, int position) {
    }
    
    private final void showEditDialog(android.content.Context context, cs.mad.flashcards.entities.Flashcard flashcard) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    private final void rePopulateCollection() {
    }
    
    public final void addItem() {
    }
    
    public final void triggerChange() {
    }
    
    public final void updateItem(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard it) {
    }
    
    public final void removeItem(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard it) {
    }
    
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcs/mad/flashcards/adapters/FlashcardAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "flashcardTitle", "Landroid/widget/TextView;", "getFlashcardTitle", "()Landroid/widget/TextView;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView flashcardTitle = null;
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getFlashcardTitle() {
            return null;
        }
    }
}