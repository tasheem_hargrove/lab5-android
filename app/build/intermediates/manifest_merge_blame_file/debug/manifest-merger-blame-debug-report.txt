1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="cs.mad.flashcards"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="24"
8-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="32" />
9-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml
10
11    <application
11-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:5:5-24:19
12        android:allowBackup="true"
12-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:6:9-35
13        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
13-->[androidx.core:core:1.7.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/f80e98fb1eb607a50e8fc819985bca22/core-1.7.0/AndroidManifest.xml:24:18-86
14        android:debuggable="true"
15        android:extractNativeLibs="false"
16        android:icon="@mipmap/ic_launcher"
16-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:7:9-43
17        android:label="@string/app_name"
17-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:8:9-41
18        android:roundIcon="@mipmap/ic_launcher_round"
18-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:9:9-54
19        android:supportsRtl="true"
19-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:10:9-35
20        android:testOnly="true"
21        android:theme="@style/Theme.Flashcards" >
21-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:11:9-48
22        <activity android:name="cs.mad.flashcards.activities.StudySetActivity" />
22-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:12:9-65
22-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:12:19-62
23        <activity
23-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:13:9-15:69
24            android:name="cs.mad.flashcards.activities.FlashcardSetDetailActivity"
24-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:14:13-66
25            android:parentActivityName="cs.mad.flashcards.activities.MainActivity" />
25-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:15:13-66
26        <activity
26-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:16:9-23:20
27            android:name="cs.mad.flashcards.activities.MainActivity"
27-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:16:19-58
28            android:exported="true" >
28-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:17:13-36
29            <intent-filter>
29-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:18:13-22:29
30                <action android:name="android.intent.action.MAIN" />
30-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:19:17-69
30-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:19:25-66
31
32                <category android:name="android.intent.category.LAUNCHER" />
32-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:21:17-77
32-->/Users/tasheem/AndroidStudioProjects/lab5-android/app/src/main/AndroidManifest.xml:21:27-74
33            </intent-filter>
34        </activity>
35
36        <service
36-->[androidx.room:room-runtime:2.4.1] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/230f147b5f7453db41ac7511e8a1e45d/room-runtime-2.4.1/AndroidManifest.xml:25:9-28:40
37            android:name="androidx.room.MultiInstanceInvalidationService"
37-->[androidx.room:room-runtime:2.4.1] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/230f147b5f7453db41ac7511e8a1e45d/room-runtime-2.4.1/AndroidManifest.xml:26:13-74
38            android:directBootAware="true"
38-->[androidx.room:room-runtime:2.4.1] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/230f147b5f7453db41ac7511e8a1e45d/room-runtime-2.4.1/AndroidManifest.xml:27:13-43
39            android:exported="false" />
39-->[androidx.room:room-runtime:2.4.1] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/230f147b5f7453db41ac7511e8a1e45d/room-runtime-2.4.1/AndroidManifest.xml:28:13-37
40
41        <provider
41-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:26:9-34:20
42            android:name="androidx.startup.InitializationProvider"
42-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:27:13-67
43            android:authorities="cs.mad.flashcards.androidx-startup"
43-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:28:13-68
44            android:exported="false" >
44-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:29:13-37
45            <meta-data
45-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:31:13-33:52
46                android:name="androidx.emoji2.text.EmojiCompatInitializer"
46-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:32:17-75
47                android:value="androidx.startup" />
47-->[androidx.emoji2:emoji2:1.0.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/0ec8049b2bd985a243ef7ec0ef83c7a1/jetified-emoji2-1.0.0/AndroidManifest.xml:33:17-49
48            <meta-data
48-->[androidx.lifecycle:lifecycle-process:2.4.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/fd7d2f93d1e3b38de17ae6dc0ae27139/jetified-lifecycle-process-2.4.0/AndroidManifest.xml:31:13-33:52
49                android:name="androidx.lifecycle.ProcessLifecycleInitializer"
49-->[androidx.lifecycle:lifecycle-process:2.4.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/fd7d2f93d1e3b38de17ae6dc0ae27139/jetified-lifecycle-process-2.4.0/AndroidManifest.xml:32:17-78
50                android:value="androidx.startup" />
50-->[androidx.lifecycle:lifecycle-process:2.4.0] /Users/tasheem/.gradle/caches/transforms-2/files-2.1/fd7d2f93d1e3b38de17ae6dc0ae27139/jetified-lifecycle-process-2.4.0/AndroidManifest.xml:33:17-49
51        </provider>
52    </application>
53
54</manifest>
